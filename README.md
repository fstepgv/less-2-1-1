# Добавление мануально роллбэка

## Description

Что изменено после 2.1:

1.В докер-композ  файле  заменили строку на
"image: $СI_REGISTRY_IMAGE/web:app_$ENVI_$SHORT_SHA"   То есть теперь мы должны задать значение переменной $SHORT_SHA

2. Во время build фазы ничего не меняется.

3. во время фазы деплоя:
сохраняем текущий билд  как PREVIOUS_SHA.    и задаем в переменную SHORT_SHA  sha  текущего пайплайна
 before_script:
    - PREVIOUS_SHA=$(docker ps | grep nginx:app_prod | awk '{print $2}'| tail -c 9) 
    - SHORT_SHA=$CI_COMMIT_SHORT_SHA

   Текущий билд пишется в артефакт
  script:
    - echo $PREVIOUS_SHA > version-file.txt ...
  artifacts:
    paths:
      - version-file.txt

 


4. Если требуется мануальный откат то используется тот же докер композ файл. Только нужно ему подсунуть прошлый $SHORT_SHA из артефакта

before_script:
    - cat version-file.txt
    - export SHORT_SHA=$(cat version-file.txt)
